import axios from 'axios';
import { createStore } from 'vuex'

export default createStore({
  state: {
    userInfo: {address: null, balance: null, assets: []},
    anothersData: [],
  },
  getters: {
    userInfo(state){
      return state.userInfo;
    },
    anothersData(state){
      return state.anothersData;
    }
  },
  mutations: {
    getMetamaskInfo(state, payload){
      state.userInfo.address = payload.address;
      state.userInfo.balance = payload.balance;
      state.userInfo.assets = payload.assets;      
    },
    getAnothersData(state, payload){
      state.anothersData = payload.data;
    },
  },
  actions: {
    async getMetamaskInfo({commit}){
      const accounts = await (window as any).ethereum.request({ method: 'eth_requestAccounts' });
      const web3 = new (window as any).Web3((window as any).ethereum);
      const userAddress =  accounts[0];
      web3.eth.getBalance(userAddress, async (error : object, balance : string) => {
        if (!error){
          const userBalance = web3.utils.fromWei(balance, 'ether');          
          await axios.post("http://localhost:5000/getData", {"address": userAddress})
          .then((res) => {
            const userAssets = res.data.data;
            commit("getMetamaskInfo", {address: userAddress, balance: userBalance, assets: userAssets});
          })
        } 
      });
    },
    getAnothersData({commit}, {address}){
      axios.post("http://localhost:5000/getAnotherUserdata", {address: address})
      .then((res) => {
        commit("getAnothersData", res.data)
      })
    }
  }
})
